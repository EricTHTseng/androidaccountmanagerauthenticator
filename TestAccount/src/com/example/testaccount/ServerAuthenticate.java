package com.example.testaccount;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;

import com.google.gson.Gson;

import android.os.AsyncTask;
import android.util.Log;

public class ServerAuthenticate {
	
	private static final String TAG = ServerAuthenticate.class.getSimpleName();
	
	private static ServerAuthenticate mInstance = null;
	
	private Token mToken = null;
	
	private String m_strTokenString = null;
	
	private String m_strTokenType = null;
	
	protected ServerAuthenticate() {
		
	}
	
	public static ServerAuthenticate getInstance() {
		if(mInstance == null) {
			mInstance = new ServerAuthenticate();
		}
		return mInstance;
	}

	public Token userLogin(String username, String password) {
		CountDownLatch latch = new CountDownLatch(1);
		
		Worker worker = new Worker("https://sdl.thermal.com/login_cli.php", username, password, latch);
		worker.start();
		
		try {
		    latch.await();
		} catch (InterruptedException e) {
			Log.e(TAG, "InterruptedException e:" + e);
			e.printStackTrace();
		}
		
		return mToken;
	}
	
	
	private class Worker extends Thread {
		private String strUri;
		private String strUsername;
		private String strPassword;
		private CountDownLatch latch;
		
		public Worker(String uri, String username, String password, CountDownLatch latch) {
			strUri = uri;
			strUsername = username;
			strPassword = password;
			this.latch = latch;
		}
		
		public void run() {
			ServerResponse sr = null;

			HttpClient httpclient = NetworkUtils.getUnsafeHttpClient();
			HttpPost httppost = new HttpPost(strUri);
			HttpResponse response = null;
			try {
				List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
						2);
				nameValuePairs.add(new BasicNameValuePair("usernameoremail",
						strUsername));
				nameValuePairs.add(new BasicNameValuePair("password",
						strPassword));

				httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

				// Execute HTTP Post Request
				response = httpclient.execute(httppost);

				BufferedReader bfr = new BufferedReader(new InputStreamReader(
						response.getEntity().getContent()));
				StringBuilder sb = new StringBuilder();
				String str = "";
				while ((str = bfr.readLine()) != null) {
					sb.append(str);
				}
				sr = new ServerResponse(response.getStatusLine()
						.getStatusCode(), sb.toString());
			} catch (ClientProtocolException e) {
				Log.e(TAG, "Exception e:" + e);
				e.printStackTrace();
			} catch (IOException e) {
				Log.e(TAG, "Exception e:" + e);
				e.printStackTrace();
			}
			
			
			if (sr == null) {
				Log.w(TAG, "server unavailable");
				// show dialog
				latch.countDown();
				return;
			}
			
			try {
				if (sr.statusCode == 200) {
					Map<String, String> map = new HashMap<String, String>();
					Gson gson = new Gson();
					map = (Map<String, String>) gson.fromJson(sr.content,
							map.getClass());

					if (map.get("result").equals("true")) {
						m_strTokenString = map.get("access_token"); // token
						m_strTokenType = map.get("token_type");
						mToken = new Token(m_strTokenString, m_strTokenType);
					} else {
						Log.w(TAG, "Invalid username and password");
					}
				} else {
					Log.e(TAG, "status code not equal to 200");
				}
			} catch (Exception e) {
				Log.e(TAG, "Exception e:" + e);
				e.printStackTrace();
				// show dialog
			} finally {
				latch.countDown();
			}
		}
	}
}
