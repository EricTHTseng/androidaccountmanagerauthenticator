package com.example.testaccount;

public class Token {
    private String m_strToken;
    
    private String m_strTokenType;
    
    public Token(String token, String tokenType) {
    	m_strToken = token;
    	m_strTokenType = tokenType;
    }
    
    public String getToken() {
    	return m_strToken;
    }
    
    public String getTokenType() {
    	return m_strTokenType;
    }
    
    public void setToken(String token) {
    	m_strToken = token;
    }
    
    public void setTokenType(String tokenType) {
    	m_strTokenType = tokenType;
    }
}
