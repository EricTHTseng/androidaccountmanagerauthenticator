package com.example.testaccount;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.SyncStateContract.Constants;
import android.text.TextUtils;
import android.util.Log;

public class AccountAuthenticator extends AbstractAccountAuthenticator {
	
	private static final String TAG = AccountAuthenticator.class.getSimpleName();

	private Context mContext;
	
	private static String s_strAccountType;
	
	public AccountAuthenticator(Context context) {
		super(context);
		Log.d(TAG, "AccountAuthenticator()");
		mContext = context;
	}

	@Override
	public Bundle editProperties(AccountAuthenticatorResponse response,
			String accountType) {
		// TODO Auto-generated method stub
		Log.d(TAG, "editProperties()");
		return null;
	}

	@Override
	public Bundle addAccount(AccountAuthenticatorResponse response,
			String accountType, String authTokenType,
			String[] requiredFeatures, Bundle options)
			throws NetworkErrorException {
		
		Log.d(TAG, "addAccount()");
		
		s_strAccountType = accountType;

		final Bundle result;
		final Intent intent;

		intent = new Intent(mContext, AuthenticatorActivity.class);
		intent.putExtra(Constant.AUTHTOKEN_TYPE, authTokenType);
		intent.putExtra(Constant.IS_ADDING_NEW_ACCOUNT, true);
		intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);

		result = new Bundle();
		result.putParcelable(AccountManager.KEY_INTENT, intent);

		return result;
	}
	
	public static String getAccountType() {
		return s_strAccountType;
	}

	@Override
	public Bundle confirmCredentials(AccountAuthenticatorResponse response,
			Account account, Bundle options) throws NetworkErrorException {
		// TODO Auto-generated method stub
		Log.d(TAG, "confirmCredentials()");
		return null;
	}

	@Override
	public Bundle getAuthToken(AccountAuthenticatorResponse response,
			Account account, String authTokenType, Bundle options)
			throws NetworkErrorException {
		// TODO Auto-generated method stub
		Log.d(TAG, "getAuthToken()");
		
		AccountManager accountManager = AccountManager.get(mContext);
		String strAuthToken = accountManager.peekAuthToken(account, authTokenType);
		
		if(TextUtils.isEmpty(strAuthToken)) {
			String strPassword = accountManager.getPassword(account);
			if(strPassword != null) {
				Token token = ServerAuthenticate.getInstance().userLogin(account.name, strPassword);
				strAuthToken = token.getToken(); 

			}
		}
		
		if(!TextUtils.isEmpty(strAuthToken)) {
			Bundle result = new Bundle();
			result.putString(AccountManager.KEY_ACCOUNT_NAME, account.name);
			result.putString(AccountManager.KEY_ACCOUNT_TYPE, account.type);
			result.putString(AccountManager.KEY_AUTHTOKEN, strAuthToken);
			return result;
		}
		
		// If we get here, then we cannot get user's password, so we need to re-prompt them for user credentials.
		// We do that by creating an intent to display our AuthenticatorActivity
		final Intent intent = new Intent(mContext, AuthenticatorActivity.class);
		intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);
		intent.putExtra(Constant.ACCOUNT_TYPE, account.type);
		intent.putExtra(Constant.AUTHTOKEN_TYPE, authTokenType);
		Bundle bundle = new Bundle();
		bundle.putParcelable(AccountManager.KEY_INTENT, intent);
		return bundle;
	}

	@Override
	public String getAuthTokenLabel(String authTokenType) {
		// TODO Auto-generated method stub
		Log.d(TAG, "getAuthTokenLabel()");
		return null;
	}

	@Override
	public Bundle updateCredentials(AccountAuthenticatorResponse response,
			Account account, String authTokenType, Bundle options)
			throws NetworkErrorException {
		// TODO Auto-generated method stub
		Log.d(TAG, "updateCredentials()");
		return null;
	}

	@Override
	public Bundle hasFeatures(AccountAuthenticatorResponse response,
			Account account, String[] features) throws NetworkErrorException {
		// TODO Auto-generated method stub
		Log.d(TAG, "hasFeatures()");
		return null;
	}

}
