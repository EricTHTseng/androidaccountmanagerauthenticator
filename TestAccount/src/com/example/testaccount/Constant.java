package com.example.testaccount;

public class Constant {
    public static final String AUTHTOKEN_TYPE = "auth.token";
    
    public static final String ACCOUNT_TYPE = "com.test";
    
    public static final String USER_PASSWORD = "user_password";
    
    public static final String IS_ADDING_NEW_ACCOUNT = "add_new_account";
}
