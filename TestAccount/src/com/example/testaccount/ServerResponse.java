package com.example.testaccount;


public class ServerResponse {
    public int statusCode;
    public String content;
    public ServerResponse(int statusCode, String content) {
        this.statusCode = statusCode;
        this.content = content;
    }

}
