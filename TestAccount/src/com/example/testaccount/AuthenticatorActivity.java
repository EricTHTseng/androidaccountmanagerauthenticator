package com.example.testaccount;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class AuthenticatorActivity extends AccountAuthenticatorActivity {
	
	private String m_strAuthTokenString;
	private String m_strAuthTokenType;
	private AccountManager mAccountManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_authenticator);

		mAccountManager = AccountManager.get(this);
	}
	
	public void onCancelClick(View v) {
		finish();
	}

	public void onSaveClick(View v) {
		EditText usernameEditText;
		EditText passwordEditText;
		final String strUsername;
		final String strPassword;
		
		usernameEditText = (EditText) findViewById(R.id.uc_txt_username);
		passwordEditText = (EditText) findViewById(R.id.uc_txt_password);
		strUsername = usernameEditText.getText().toString();
		strPassword = passwordEditText.getText().toString();

		// check username and password on server side
		new AsyncTask<Void, Void, Intent>() {
			@Override
			protected Intent doInBackground(Void... params) {
				Token token = ServerAuthenticate.getInstance().userLogin(strUsername, strPassword);
				m_strAuthTokenString = token.getToken();
				m_strAuthTokenType = token.getTokenType();
				
				Intent intent = new Intent();
				intent.putExtra(AccountManager.KEY_ACCOUNT_NAME, strUsername);
				intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, Constant.ACCOUNT_TYPE);
				intent.putExtra(AccountManager.KEY_AUTHTOKEN, m_strAuthTokenString);
				intent.putExtra(Constant.USER_PASSWORD, strPassword);
				return intent;
			}

			@Override
			protected void onPostExecute(Intent intent) {
				finishLogin(intent);
			}
		}.execute();
	}
	
	private void finishLogin(Intent intent) {
		String strAccount = intent.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
		String strPassword = intent.getStringExtra(Constant.USER_PASSWORD);
		Account account = new Account(strAccount, intent.getStringExtra(AccountManager.KEY_ACCOUNT_TYPE));
		
		if(getIntent().getBooleanExtra(Constant.IS_ADDING_NEW_ACCOUNT, false)) {
			String strAuthToken = intent.getStringExtra(AccountManager.KEY_AUTHTOKEN);
			
			// add this account to AccountManager
			mAccountManager.addAccountExplicitly(account, strPassword, null);
			mAccountManager.setAuthToken(account, m_strAuthTokenType, strAuthToken);
		}
		else {

			mAccountManager.setPassword(account, strPassword);
		}
		this.setAccountAuthenticatorResult(intent.getExtras());
		this.setResult(RESULT_OK, intent);
		this.finish();
	}

}
